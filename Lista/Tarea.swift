//
//  Tarea.swift
//  Lista
//
//  Created by Alexandre Martinez Olmos on 4/11/14.
//  Copyright (c) 2014 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

var array: [Tarea] = Array()

class Tarea: NSObject {
    var titulo: String
    var descripcion: String
    var realizada: Bool = false
    var imagen: UIImage = UIImage(named: "Apple.png")!
    
    init(titulo: String, descripcion: String) {
        self.titulo = titulo
        self.descripcion = descripcion
    }
    
}
