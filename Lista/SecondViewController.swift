//
//  SecondViewController.swift
//  Lista
//
//  Created by Alexandre Martinez Olmos on 4/11/14.
//  Copyright (c) 2014 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var primerTextField: UITextField!
    @IBOutlet weak var segundoTextField: UITextField!
    @IBOutlet weak var crearButton: UIButton!
    
   
    @IBAction func crearTarea(sender: UIButton) {
        var tarea = Tarea(titulo:primerTextField.text, descripcion:segundoTextField.text)
        array.append(tarea)
        self.tabBarController?.selectedIndex = 0
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //Para ocultar el teclado cuando pulsas fuera del TextField
    override func touchesBegan(touches: NSSet, withEvent event:
        UIEvent) {
            self.view.endEditing(true)

    }
}



