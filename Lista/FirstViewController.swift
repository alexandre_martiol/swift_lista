//
//  FirstViewController.swift
//  Lista
//
//  Created by Alexandre Martinez Olmos on 4/11/14.
//  Copyright (c) 2014 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {

    @IBOutlet weak var miTabla: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection
        section: Int) -> Int {
            //Debe devolver el tamaño del Array de tareas
            return array.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath
        indexPath: NSIndexPath) -> UITableViewCell{
            var cell: CeldaPersonalizada =
            tableView.dequeueReusableCellWithIdentifier("celdaPersonalizada"
                ) as CeldaPersonalizada
            
            //Aquí tienes que rellenar los IBOutlets de CeldaPersonalizada con los atributos de la tarea que ocupa la posición indexPath.row en el Array
            
            cell.labelSuperior.text = array[indexPath.row].titulo
            
            cell.labelInferior.text = array[indexPath.row].descripcion
            
            cell.imagenGrande.image = array[indexPath.row].imagen
            
            return cell
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection
        section: Int) -> String? {
            //Debe devolver el título de la tabla (el que quieras ponerle)
            return "Mis Tareas"
    }


}

