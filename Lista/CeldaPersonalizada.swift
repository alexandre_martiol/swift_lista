//
//  CeldaPersonalizada.swift
//  Lista
//
//  Created by Alexandre Martinez Olmos on 4/11/14.
//  Copyright (c) 2014 Alexandre Martinez Olmos. All rights reserved.
//

import UIKit

class CeldaPersonalizada: UITableViewCell {
    
    @IBOutlet weak var imagenGrande: UIImageView!
    @IBOutlet weak var labelSuperior: UILabel!
    @IBOutlet weak var labelInferior: UILabel!
    @IBOutlet weak var imagenPeque: UIImageView!
    
    override init(style: UITableViewCellStyle, reuseIdentifier:
        String?){
            super.init(style: style, reuseIdentifier:
                reuseIdentifier)
    }
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder) 
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
